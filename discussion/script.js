// Javascript Synchronous vs Asynchronous

// Synchronous Javascript
/*
	In Synchronous operations, tasks are performed one at a time and only when one is completed, the following is unblocked.

	Javascript by default is synchronous.
*/
console.log("Hello World!");
// console.log("Hello Again!");
/*for(let i = 0; i <= 1500; i++){
	console.log(i);
}*/
// Blocking is when the execution of additional JS process must wait until the operation completes.
// Blocking is just the code is slow.
console.log("Goodbye!");

// Asynchronous Javascript

// We can classify most asynchronous JS operationns with 2 primary triggers.
// 1. Browser API / Web API events or functions. These include methods like setTimeout(), or event handlers like onclick, mouse over, scroll and many more

/*function printMe(){
	console.log("Print me");
}

setTimeout(printMe, 5000);*/

function print(){
	console.log("Print meeeeeeeeeeee");
}

function test(){
	console.log("test");
}

// setTimeout(print, 4000);
test();

/*function f1(){
	console.log("f1");
}
function f2(){
	console.log("f2");
}
function main(){
	console.log("main");
	setTimeout(f1, 0);
	new Promise((resolve, reject) => resolve("I am a promisess")).then(resolve => console.log(resolve));
	f2();
}

main();
*/
/*Callback Queue (Data Structure)*/
/*
	In Javascript, we have a queue of call back functions. It is called a callback queue or task queue.

	A queue data structure is "First-In-First-Out" (Pila sa MRT)
*/
/*
	Job queue
	Every time a promise occurs in the code, the executor function gets into the job queue. The event loop works as usual, to look into the queues but gives priority to the job queue items over the callback queue items when the stack is free. (Paunahang matapos/Job order)
*/

// 2. Promises. A unique Javascript object that allows us to perform asynchronous objects.

// The promises object represents the eventual completion (or failure) of an asynchronous operation and its resulting value.

/*new Promise((resolve, reject) => 
		resolve("I am a promise")
	).then(resolve => console.log(resolve));*/

// REST API using a JSON placeholder

// Getting all posts (GET method)

// Fetch - allows us to asynchronously request for a resource (data)
// We use the promise for async.

/*
	Syntax:
	fetch("url", {optionalObjects}).then(response => response.json()).then(data);

	optional object(s) - contains additional information about our requests such as the method, the body, and the headers of our request.

	
*/ 

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));
// A promise may be in one of 3 possible states: pending, fulfilled, or rejected.

// fetch("https://jsonplaceholder.typicode.com/posts",)
// 	.then(response => response.json())/*parse the response as json*/
// 	.then(data => console.log(data)/*process the results*/);

// async / await function
/*async function fetchData(){
	// wait for the "fetch" method to complete and stores the value in a variable name.

	let result = await fetch("https://jsonplaceholder.typicode.com/posts");
	console.log(result);
	console.log(typeof result);

	// converts the data form the "response"

	let json = await result.json();
	console.log(json);
	console.log(typeof json);
}

fetchData();*/

fetch("https://jsonplaceholder.typicode.com/posts/1",)
	.then(response => response.json())
	.then(data => console.log(data));

// Creates a new post using fetch()
fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	// Sets the content/body of the "request" object to be sent to the backend
	body: JSON.stringify({
		title: "New Post",
		body: "Hello again",
		userId: 2
	})
})
.then(res => res.json())
.then(data => console.log(data));

// updating a post
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello again",
		userId: 1
	})
})
.then(res => res.json())
.then(data => console.log(data));

// PUT method - updates the whole document. modifying resource where the client sends data that updates the entire resource.

// The PATCH method applies partial update to resource.


// delete a post
fetch('https://jsonplaceholder.typicode.com/posts/1', {method: 'DELETE'}).then(res=> res.json()).then(data=>console.log(data));

// Filtering posts
// The information sent via the url can be done by adding the question mark symbol (?)

/*
	Syntax
	Individual parameters: "url?parameterName=value"
	Multiple parameters:
	"url?parameterName=value&parameterNameB=valueB..."
*/
fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
.then(res => res.json())
.then(data => console.log(data));

// Retrieve a nested/related comments to our posts.
// Retrieving comments for a specific post(/posts/:id/comments)

fetch("https://jsonplaceholder.typicode.com/posts/2/comments")
.then(res => res.json())
.then(data => console.log(data))