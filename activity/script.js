// Get all data thru fetch

fetch("https://jsonplaceholder.typicode.com/todos")
	.then(res => res.json())
	.then(data => console.log(data));


// Get all data and display all items
fetch("https://jsonplaceholder.typicode.com/todos")
	.then(res => res.json())
	.then(data => {
		let titles = data.map((title) => title.title);

		console.log(titles);
	});

// Get specific data thru fetch (GET)
fetch("https://jsonplaceholder.typicode.com/todos/1/")
	.then(res => res.json())
	.then(data => {
		console.log(`Title: ${data.title}`);
		console.log(`Status: ${data.completed}`);
	});

// Add data thru fetch (POST)
fetch("https://jsonplaceholder.typicode.com/todos/",{
	method: "POST",
	headers: {"Content-Type": "application/json"},
	body: JSON.stringify({
		userId: 5,
		title: "Clean the house",
		completed: false
	})
})
	.then(res => res.json())
	.then(data => console.log(data));

// Edit data thru fetch (PUT)
// Before
fetch("https://jsonplaceholder.typicode.com/todos/3/")
	.then(res => res.json())
	.then(data => console.log(data));
// After
fetch("https://jsonplaceholder.typicode.com/todos/3",{
	method: "PUT",
	headers: {"Content-Type": "application/json"},
	body: JSON.stringify({
		userId: 7,
		title: "Learn Python",
		completed: true,
		dateCompleted: "2021-03-12"
	})
})
	.then(res => res.json())
	.then(data => console.log(data));

// Edit data thru fetch (PATCH)
// Before
fetch("https://jsonplaceholder.typicode.com/todos/4/")
	.then(res => res.json())
	.then(data => console.log(data));
// After
fetch("https://jsonplaceholder.typicode.com/todos/4",{
	method: "PATCH",
	headers: {"Content-Type": "application/json"},
	body: JSON.stringify({
		userId: 8,
		title: "Play Cities Skylines"
	})
})
	.then(res => res.json())
	.then(data => console.log(data));

// Delete Data
// Before
fetch("https://jsonplaceholder.typicode.com/todos/5/")
	.then(res => res.json())
	.then(data => console.log(data));

fetch("https://jsonplaceholder.typicode.com/todos/5/", {
	method: "DELETE"
})
	.then(res => res.json())
	.then(data => console.log(data));
